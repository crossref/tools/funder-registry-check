# Funder Registry Check

A quick health-check for a Funder RDF file. Detects Funders that are referenced in the file, but not defined. Run this prior to importing into the Funder Registry.

## To Run

To run on a Funder ZIP file, or the RDF file found in a zip file.
```shell
java -jar funder-registry-check-1.0-SNAPSHOT.jar «ZIP FILE»
```

e.g. for the ZIP file

```shell
java -jar funder-registry-check-1.0-SNAPSHOT.jar /Users/jcarberry/funders/FundRef_v1910-11.zip
```

or the unzipped RDF file

```shell
java -jar funder-registry-check-1.0-SNAPSHOT.jar /Users/jcarberry/funders/FundRef_v1910-11_19101123_nowrapper.rdf
```

This tool will output the list of missing funders. It both shows the Concepts URIs as found in the file, and also just as funder IDs: 

```
Missing concepts:
http://data.elsevier.com/vocabulary/SciValFunders/100013058
http://data.elsevier.com/vocabulary/SciValFunders/501100022550
http://data.elsevier.com/vocabulary/SciValFunders/7710
As funder IDs:
100013058
501100022550
7710
```

You can send these back to indicate bugs in the data.

## To compile

Ask a developer to do this.
```shell
mvn package
```

