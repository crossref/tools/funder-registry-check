import org.apache.jena.rdf.model.*
import java.io.FileInputStream
import java.io.InputStream
import java.util.zip.ZipFile

const val SCI_VAL_PREFIX = "http://data.elsevier.com/vocabulary/SciValFunders/"
const val SKOS_PREFIX = "http://www.w3.org/2004/02/skos/core#"

/**
 * Read a Funder Registry input file, either as an RDF-XML file, or a ZIP file containing that file.
 */
fun main(args: Array<String>) {
    val filename = args.getOrNull(0)
    if (filename == null) {
        println("Error! Please supply filename of RDF file or ZIP of RDF file.")
    } else {
        if (filename.endsWith(".zip")) {
            println("Found zip file $filename")
            val file = ZipFile(filename)
            file.entries().asSequence().forEach { entry ->
                file.getInputStream(entry).use {
                    check(it, entry.name)
                }
            }
        } else {
            FileInputStream(filename).use {
                check(it, filename)
            }
        }
    }
}

fun removePrefixes(resource: Resource) = resource
    .toString()
    .replaceFirst(SCI_VAL_PREFIX, "")
    .replaceFirst(SKOS_PREFIX, "")

fun check(stream: InputStream, filename: String) {
        println("Checking $filename...")
        val model = ModelFactory.createDefaultModel()

        model.read(stream, filename)

    /* Funders are represented as 'Concept' in the SKOS vocabulary.
    * We're checking that every funder referenced is also defined as a Concept.
     */
    val skosConcept = model.createProperty("http://www.w3.org/2004/02/skos/core#Concept")

    // The set of funder relationships we want to look for. Any funder mentioned by one of these will be checked.
    val interFunderRelationships = listOf(
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/affilWith",
        "http://www.w3.org/2004/02/skos/core#narrower",
        "http://www.w3.org/2004/02/skos/core#broader",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/incorporates",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/incorporatedInto",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/continuationOf",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/renamedAs",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/mergerOf",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/mergedWith",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/splitInto",
        "http://www.elsevier.com/xml/schema/grant/grant-1.2/splitFrom",
        "http://purl.org/dc/terms/replaces",
        "http://purl.org/dc/terms/isReplacedBy"
    ).map { model.createProperty(it) }

    // the set of relationships used to represent hierarchy.
    val hierarchyRelationships = listOf(
        "http://www.w3.org/2004/02/skos/core#narrower",
        "http://www.w3.org/2004/02/skos/core#broader"
    ).map { model.createProperty(it) }


    checkMissingConcepts(model, skosConcept, interFunderRelationships)
    checkCycles(model, skosConcept, hierarchyRelationships)

    println("Done")
}

/** Given a Resource, follow all links recursively.
 * Throw an error if a cycle was found.
 */
private fun findCycles(model: Model, item: Resource, path: List<Statement>, relationship: Property) {
    // We're interested in a few relationship types to follow.
    // But only try one at a time, we're interested only in cycles of the same kind of predicate.
    val allRelated = model.listStatements(SimpleSelector(item, relationship, null as Any?)).asSequence()

    for (related in allRelated) {
        val obj = related.`object`.asResource()
        if (path.any { it.`object`.asResource() == obj }) {
            println("Found cycle:")
            path.forEach {
                println("${removePrefixes(it.subject.asResource())} ${removePrefixes(it.predicate)} ${removePrefixes(it.resource.asResource())}")
            }
            println()
        } else {
            findCycles(model, obj, path + related, relationship)
        }
    }
}

private fun checkCycles(model: Model, skosConcept: Property, interFunderRelationships: List<Property>) {
    val allFunders = model.listStatements(SimpleSelector(null, null, skosConcept)).asSequence()

    allFunders.forEach {
        val funder = it.subject.asResource()

        for (relationshipType in interFunderRelationships) {
            findCycles(model, funder, emptyList(), relationshipType)
        }

    }
}

private fun checkMissingConcepts(model: Model, skosConcept: Property, interFunderRelationships: List<Property>) {

    /**
     * Find statements matching the selector that references subject or object Funders, but those funders aren't defined.
     */
    fun findMissingConcepts(
        statementSelector: SimpleSelector,
    ): List<Resource> = model.listStatements(statementSelector).asSequence().flatMap {

        val subject = it.subject
        val foundAsSubject =
            model.listStatements(SimpleSelector(subject, null, skosConcept)).iterator().asSequence().toList()

        val obj = it.`object`.asResource()
        val foundAsObject =
            model.listStatements(SimpleSelector(obj, null, skosConcept)).iterator().asSequence().toList()

        if (foundAsSubject.isEmpty()) {
            listOf(subject)
        } else {
            emptyList()
        } + if (foundAsObject.isEmpty()) {
            listOf(obj)
        } else {
            emptyList()
        }

    }.asSequence().filterNotNull().toList()


    // All Funders identified as missing in one of the given relationship types.
    val allMissing = interFunderRelationships.flatMap {
        val selector = SimpleSelector(null, it, null as Any?)
        findMissingConcepts(selector)
    }.sortedBy { it.toString() }

    println("Found ${allMissing.count()} referenced but missing funder IDs:")
    allMissing.forEach {
        println(removePrefixes(it))
    }
    println("\n")
}
